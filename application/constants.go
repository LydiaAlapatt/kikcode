package main

const degreeCircle = 360
const allBitsForCircumference = 52
const bitsleft = 12
const allBitsInt64 = allBitsForCircumference + bitsleft

//number of bits for circumference 1 to 6
const c1Bits = 3
const c2Bits = 4
const c3Bits = 8
const c4Bits = 10
const c5Bits = 12
const c6Bits = 15

//shiftsums for getting the single circumferences from the userID
const shiftsumc1 = bitsleft
const shiftsumc2 = shiftsumc1 + c1Bits
const shiftsumc3 = shiftsumc2 + c2Bits
const shiftsumc4 = shiftsumc3 + c3Bits
const shiftsumc5 = shiftsumc4 + c4Bits
const shiftsumc6 = shiftsumc5 + c5Bits

//bitmasks for the circumferences to check if there is a one or a zero
const bitmaskc1 uint64 = 0x4
const bitmaskc2 uint64 = 0x8
const bitmaskc3 uint64 = 0x80    //2^7 = 128
const bitmaskc4 uint64 = 0x200   //2^9 = 512
const bitmaskc5 uint64 = 0x800   // 2^11 = 2048
const bitmaskc6 uint64 = 0x4000 //2^14 = 16384

//degree per bit of Circumference
const c1dpsec = degreeCircle / c1Bits
const c2dpsec = degreeCircle / c2Bits
const c3dpsec = degreeCircle / c3Bits
const c4dpsec = degreeCircle / c4Bits
const c5dpsec = degreeCircle / c5Bits
const c6dpsec = degreeCircle / c6Bits

type Segment struct{
	circumferenceNumber int
	start_degree        int
	end_degree          int
	color               string
}

type Circumference struct {
	number             int
	shiftsum           int
	sectorbits         int
	bitmask            uint64
	bitsForCoordinates uint64
	degree             int
}

func getShiftSumsOfCircumference() []Circumference{
	 return []Circumference{
		 {number: 1, shiftsum: shiftsumc1, sectorbits: c1Bits, bitmask: bitmaskc1, degree: c1dpsec},
		 {number: 2, shiftsum: shiftsumc2, sectorbits: c2Bits, bitmask: bitmaskc2, degree: c2dpsec},
		 {number: 3, shiftsum: shiftsumc3, sectorbits: c3Bits, bitmask: bitmaskc3, degree: c3dpsec},
		 {number: 4, shiftsum: shiftsumc4, sectorbits: c4Bits, bitmask: bitmaskc4, degree: c4dpsec},
		 {number: 5, shiftsum: shiftsumc5, sectorbits: c5Bits, bitmask: bitmaskc5, degree: c5dpsec},
		 {number: 6, shiftsum: shiftsumc6, sectorbits: c6Bits, bitmask: bitmaskc6, degree: c6dpsec},
	 }

}


