package main

import (
	"fmt"
	"strconv"
)

func stringToInteger(userId string) uint64 {
	converted, err := strconv.Atoi(userId)
	if err != nil {
		fmt.Println(err)
	}
	return uint64(converted)
}

//after reversion: bit at index i should be at index 63-i
func reverseBitsOfUserId(n uint64) uint64 {
	res := uint64(0)
	power := uint64(63)
	for n != 0 { //stop when n equals zero
		//use AND operator with n and 1 to get the value of the right-most bit
		//shift result of AND-operation to the left with value of power => bit at position i is then at position 63-i
		//add it to res
		res += (n & 1) << power
		//shift n 1 bit to the right to get the next bit
		n = n >> 1
		//reduce power
		power -= 1
	}
	return res
}

//shift userId in order to get bits for the single circumferences
func getBitsPerCircumferences(n uint64) []Circumference {
	shiftBits := getShiftSumsOfCircumference()
	temp := n

	for index, value := range shiftBits {
		//shift n to the left so that the bits for the circumference start from the most significant bit downwards
		temp = n << value.shiftsum
		//shift temp to the right in order to get the bits for a single circumference with leading zeros
		value.bitsForCoordinates = temp >> (allBitsInt64 - value.sectorbits)
		shiftBits[index] = value
	}
	return shiftBits
}

//generate all the segments for the circumferences that are colored white
func generateSegments(n uint64) [][]*Segment {
	circumferences := getBitsPerCircumferences(n)
	var temp uint64 = 0
	var singleBit uint64 = 0
	circumferencesWithSegments := [][]*Segment{}

	for _, value := range circumferences {
		//reset counters, degrees and segments for the next circumference
		counterForWhite := 0
		counterForBlack := 0
		startDegreeBlack := 0
		endDegreeBlack := 0
		var segments []*Segment = []*Segment{}

		//calculate segments for each circumference
		for i := 0; i < value.sectorbits; i++ {
			//shift bitsForCoordinates gradually over the bitmask, use AND in order to get a single bit
			temp = value.bitsForCoordinates << i
			singleBit = temp & value.bitmask
			if singleBit == value.bitmask { //in case singleBit contains a 1 => bit is colored white
				counterForWhite++
				counterForBlack = 0
				if counterForWhite == 1 && len(segments) == 0 && endDegreeBlack >= 0 { //first white segment or very first segment of the circumference
					//create a new segment, alter the start and end degree and append segment to segments
					var s = &Segment{circumferenceNumber: value.number, start_degree: endDegreeBlack, end_degree: value.degree, color: "white"}
					s.end_degree += s.start_degree
					segments = append(segments, s)
				} else if counterForWhite == 1 && segments[len(segments)-1] != nil { //if white starts again and there are already white segments
					//create a new segment, alter the start and end degree and append segment to segments
					var s = &Segment{circumferenceNumber: value.number, start_degree: endDegreeBlack, end_degree: value.degree, color: "white"}
					s.end_degree += s.start_degree
					segments = append(segments, s)
				} else if counterForWhite > 0 && len(segments) > 0 { //multiple white segments in a row
					//get the end_degree of the last segment and add to it value.degree
					segments[len(segments)-1].end_degree += value.degree
				}
			} else if singleBit == uint64(0) { //in case singleBit contains a 0 => bit is colored black
				counterForWhite = 0
				counterForBlack++
				if counterForBlack == 1 && len(segments) == 0 { //very first segment of the circumference
					//set degrees for black segments
					startDegreeBlack = 0
					endDegreeBlack = value.degree + startDegreeBlack
				} else if counterForBlack == 1 && segments[len(segments)-1] != nil { //if black starts again after a white segment
					//adapt degrees for black segments
					startDegreeBlack = segments[len(segments)-1].end_degree
					endDegreeBlack = value.degree + startDegreeBlack
				} else if counterForBlack > 0 && len(segments) >= 0 { //multiple black segments in a row
					endDegreeBlack = endDegreeBlack + value.degree
				}
			}

		}
		//add segments for a single circumference to all segments
		circumferencesWithSegments = append(circumferencesWithSegments, segments)
	}

	return circumferencesWithSegments
}

//check if there are segments that can be treated as one segment and are joined together
func checkDegreeOfSegments(circumferencesWithSegments [][]*Segment) {

	for index, segments := range circumferencesWithSegments {
		//check segments for a single circumference from the last segment on, going backwards
		for i := len(segments) - 2; i >= 0; i-- {
			//if end_degree of one segment and start_degree + 360° of a preceding segment are the same, the segments are joined together
			if segments[i+1].end_degree == segments[i].start_degree+degreeCircle {
				//check if condition start_degree < end_degree applies, if not, add 360° to the end_degree of the preceding segment
				if segments[i+1].start_degree > segments[i].end_degree {
					segments[i+1].end_degree = segments[i].end_degree + degreeCircle
					segments = append(segments[:i], segments[i+1:]...)
					//overwrite segments slice at given index with the changed slice as range returns a copy
					circumferencesWithSegments[index] = segments
					//start from the beginning
					i = len(segments) - 2

				}
			}
		}
	}

}

//transfer 2D slice with *Segment to the required 3D slice with integers
func transferToListNotation(circumferencesWithSegments [][]*Segment) [][][]int {
	store := [][][]int{}

	for _, segments := range circumferencesWithSegments {
		for _, segment := range segments {
			var storesegments = [][]int{}
			var degreeStart = []int{segment.circumferenceNumber, segment.start_degree}
			var degreeEnd = []int{segment.circumferenceNumber, segment.end_degree}
			storesegments = append(storesegments, degreeStart, degreeEnd)
			store = append(store, storesegments)
		}
	}
	return store
}

func kikCode(userId string) [][][]int {
	reverseId := reverseBitsOfUserId(stringToInteger(userId))
	//shift 12 bits to the left to get rid of ending zeros and get leading zeros since 52-bit ID is stored in int64
	segments := generateSegments(reverseId >> 12)
	checkDegreeOfSegments(segments)
	return transferToListNotation(segments)
}
