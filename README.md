# Kikcode: Application for Bolinda Labs

This is a project for my application for Bolinda Labs. The task is to create a kikCode.
The programming language is Go.

## Task description
The objective is to convert a given userId (string) to a kikCode. The Code consists of 6 circumferences. 
Each circumference has a specific number of bits. 
I work with the binary representation of the userId (52 bits).
The binary number is extended by leading zeros, reversed and then mapped to the Code. 

|Circumference | Bits/Sectors | Degree per Bit: 360°/Bits|
|----|----|----|
|1|first 3|120|
|2|next 4|90|
|3|next 8|45|
|4|next 10|36|
|5|next 12|30|
|6|last 15|24|

![circle_coordinates](image/polar_coordinates_colored_circle.jpg) 
 
The implemented function returns a list of colored segments' coordinates in the polar coordinate system with the center
at the center of the circumferences. 

* direction is counterclockwise.
* A one in the number is colored white, a zero is colored black.
* Return value: [[[r, start_angle], [r, end_angle]]] array.array.array.integer
* start_angle < end_angle
* Everytime the start_angle is greater than the end_angle a full circle (360°) needs to be added to the end_angle.




